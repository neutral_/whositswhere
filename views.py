# -*- coding: utf-8 -*-
from random import randint
from time import strptime, time
import datetime
from flask import request, redirect
from flask.ext.admin import expose, AdminIndexView, BaseView
from flask.ext.admin.form import rules
from flask.ext.admin.contrib.mongoengine import ModelView
from flask.ext.admin.contrib.mongoengine.helpers import make_thumb_args
from flask.ext.admin.contrib.mongoengine.typefmt import grid_image_formatter, DEFAULT_FORMATTERS
from flask.ext.admin.helpers import get_redirect_target, get_form_data
from flask.ext.login import current_user, login_required
from markupsafe import Markup
from mongoengine import GridFSProxy
from bson.objectid import ObjectId
import os

from wtforms.fields.html5 import DateField
from wtforms_components import TimeField
from wtforms import TextAreaField, validators

from config import APP_ROOT
from forms import siting_form_factory, SitingForm, ReportForm, TeacherSelectField, MyTimeField, SitingTimeForm, \
    siting_time_form_factory
from models import User, Role, Subject, Group, SubjTable, Classroom, Desk, Siting, Semester
from flask_admin.contrib.mongoengine.filters import FilterLike
from filters import NameFilter, MyFilterLike
from base import TranslateModelView
import json
import uuid
import re

roles = Role.objects
DEFAULT_FORMATTERS.update({
    GridFSProxy: grid_image_formatter
})


class SitingView(ModelView):
    column_list = ['subject', 'group', 'classroom', 'students_present_count', 'students_not_present_count', 'pair_count', 'date', 'time']

    form_excluded_columns = ('students_present', 'students_not_present', 'tables')

    column_default_sort = '-id'

    form_args = dict(
        students_present_count=dict(label=u"Количество присутствующих"),
        students_not_present_count=dict(label=u"Количество отсутствующих"),
        subject=dict(label=u"Предмет"),
        group=dict(label=u"Группа"),
        classroom=dict(label=u'Аудитория'),
        date=dict(label=u'Дата'),
        time=dict(label=u'Время'),
        pair_count=dict(label=u'Количество пар')
    )

    column_labels = dict(
        students_present_count=u"Количество присутствующих",
        students_not_present_count=u"Количество отсутствующих",
        subject=u"Предмет",
        group=u"Группа",
        classroom=u'Аудитория',
        date=u'Дата',
        time=u'Время',
        pair_count=u'Количество пар'
    )

    column_formatters = dict(
        date=lambda v, c, m, p: datetime.datetime.strftime(m.date, '%d.%m.%Y')
    )

    def __init__(self):
        super(SitingView, self).__init__(model=Siting, name=u'Рассадка', category=None, endpoint=None,
                                         url=None, static_folder=None, menu_class_name=None,
                                         menu_icon_type=None, menu_icon_value=None)

    def is_accessible(self):
        return current_user.has_role('Admin') or current_user.has_role('Teacher')

    @expose('/edit/', methods=('GET', 'POST'))
    def edit_view(self):
        return_url = get_redirect_target() or self.get_url('.index_view')

        if not self.can_create:
            return redirect(return_url)

        if request.method == 'GET':
            obj = Siting.objects.get(id=request.args.get('id'))
            form = siting_form_factory(obj)

            data = dict(
                siting_id=unicode(obj.id),
                classroom=unicode(obj.classroom.id),
                group=unicode(obj.group.id),
                subject=unicode(obj.subject.id),
                tables={},
                start_time=None,
                end_time=None,
                semester=unicode(obj.semester.id),
                date=unicode(datetime.datetime.strftime(obj.date, '%d.%m.%Y')),
                time=unicode(obj.time),
                pair_count=unicode(obj.pair_count)
            )
            data['tables'] = {desk: unicode(student) for desk, student in obj.tables.iteritems()}

            return self.render(template='siting.html', initial_data=data, form=form, return_url=return_url)

        if request.method == 'POST':
            try:
                self.save_siting(request.json)
            except Exception, e:
                return json.dumps({'status': 'failed'}), 400
            else:
                return json.dumps({'status': 'success'})

    @expose('/data/<dt>', methods=('GET', 'POST'))
    def get_data(self, dt=None):
        if dt:
            obj = Siting.objects.get(id=dt)
            data = dict(
                classroom=unicode(obj.classroom.id),
                group=unicode(obj.group.id),
                tables=dict(obj.tables),
                # start_time=obj.start_time if obj.start_time else None,
                # end_time=obj.start_time if obj.start_time else None,
                subject=unicode(obj.subject.id),
                semester=unicode(obj.semester.id),
            )
            data['desks'] = []
            for desk in obj.desks:
                data['desks'].append({
                    'id': unicode(desk.id),
                    'x': unicode(desk.x),
                    'y': unicode(desk.y),
                })
            return json.dumps(data)

    @expose('/class_data/<room_id>', methods=('GET', 'POST'))
    def get_class_data(self, room_id=None):
        classroom = Classroom.objects.get(id=room_id)
        classroom_data = dict(
            id=unicode(classroom.id),
            horizontal_length=int(classroom.horizontal_length),
            vertical_length=int(classroom.vertical_length),
            board_position=int(classroom.board_position or 0)
        )
        classroom_data['desks'] = []
        for desk in classroom.desks:
            classroom_data['desks'].append({
                'id': unicode(desk.id),
                'x': unicode(desk.x),
                'y': unicode(desk.y),
            })
        return json.dumps(classroom_data)

    @expose('/group-data/<group_id>', methods=('GET',))
    def get_group_data(self, group_id=None):
        group = Group.objects.get(id=group_id)
        group_data = dict(
            id=unicode(group.id),
            name=unicode(group.name),
            year=int(group.year),
            students=list()
        )
        students = User.objects(group=group)
        for student in students:
            student_data = {
                'id': unicode(student.id),
                'last_name': unicode(student.last_name),
                'first_name': unicode(student.first_name),
                'second_name': unicode(student.second_name),
                'photo': None,
                'login': unicode(student.login),
                'active': int(student.active),
                'group': unicode(group.id)
            }
            if student.photo.grid_id:
                student_data['photo'] = self.get_url('.api_file_view', **make_thumb_args(student.photo))
            group_data['students'].append(student_data)
        return json.dumps(group_data)

    @expose('/new/', methods=('GET', 'POST'))
    def create_view(self):
        return_url = get_redirect_target() or self.get_url('.index_view')

        if not self.can_create:
            return redirect(return_url)

        if request.method == 'GET':
            form = siting_form_factory()
            return self.render(template='siting.html', form=form, return_url=return_url)

        if request.method == 'POST':
            try:
                self.save_siting(request.json)
            except Exception, e:
                return json.dumps({'status': 'failed'}), 400
            else:
                return json.dumps({'status': 'success'})

    @staticmethod
    def save_siting(data):
        classroom_id = data['classroom']
        group_id = data['group']
        semester_id = data['semester']
        subject_id = data['subject']
        pair_count = data['pair_count']
        date = data['date'] and datetime.datetime.strptime(data['date'], "%Y-%m-%d").date() or None
        pair_time = data['time']
        sitings = data['sitings']

        subj_table = SubjTable.objects(group=group_id, subject=subject_id, semester=semester_id).first()
        teacher = subj_table and subj_table.teacher or None

        if 'siting_id' not in data:
            siting = Siting(
                subject=subject_id,
                classroom=classroom_id,
                group=group_id,
                teacher=teacher,
                start_time=None,
                end_time=None,
                semester=semester_id,
                date=date,
                time=pair_time,
                pair_count=pair_count
            )
        else:
            siting = Siting.objects.get(id=data['siting_id'])
            siting.subject = Subject.objects.get(id=subject_id)
            siting.classroom = Classroom.objects.get(id=classroom_id)
            siting.group = Group.objects.get(id=group_id)
            siting.teacher = teacher
            siting.semester = Semester.objects.get(id=semester_id)
            siting.date = date
            siting.time = pair_time
            siting.pair_count = pair_count
            

        tables = {}
        for siting_data in sitings:
            if 'student' in siting_data:
                tables[siting_data['id']] = ObjectId(siting_data['student'])
        siting.tables = tables

        present = User.objects(group=group_id, id__in=siting.tables.values())
        not_present = User.objects(group=group_id, id__nin=siting.tables.values())

        siting.students_present = present
        siting.students_not_present = not_present
        siting.students_not_present_count = len(not_present)
        siting.students_present_count = len(present)
        siting.save()

    def grid_image_formatter(self, value):
        if not value.grid_id:
            return ''

        return Markup(
            ('<div class="image-thumbnail">' +
                '<img src="%(thumb)s"/>' +
             '</div>') %
            {
                'thumb': self.get_url('.api_file_view', **make_thumb_args(value)),
            })

    @expose('/choices/', methods=('GET', 'POST'))
    def choices(self):
        data = {}
        if request.method == 'GET':
            if request.args:
                s = Semester.objects.get(id=request.args.get('semester'))
                time_options = ['8:00', '9:30', '11:00', '12:40', '14:20', '15:50', '17:20']
                data['start_date'] = datetime.datetime.strftime(s.start_time, '%Y-%m-%d')
                data['end_date'] = datetime.datetime.strftime(s.end_date, '%Y-%m-%d')
                data['time_options'] = time_options
        if request.method == 'POST':
            role = roles.filter(name='Student')[0].id
            try:
                request_data = json.loads(request.form.keys()[0])
            except:
                request_data = request.form
            data['subject'] = request_data['subject']
            data['end_time'] = request_data.get('end_time')
            data['start_time'] = request_data.get('start_time')
            data['group'] = request_data.get('group')
            data['semester'] = request_data.get('semester')

            data['students'] = [
                {'id': unicode(i.id), 'name': unicode(i), 'image': self.grid_image_formatter(i.photo)}
                for i in User.objects(roles=role, group=Group.objects(
                    id=request_data['group']).first().id)
            ]
            classroom = Classroom.objects(id=request_data['classroom']).first()
            data['classroom'] = dict(
                id=unicode(classroom.id),
                horizontal_length=int(classroom.horizontal_length),
                vertical_length=int(classroom.vertical_length),
                board_position=int(classroom.board_position)
            )
            form, min, max = siting_time_form_factory(request_data.get('semester'))
            data['form'] = self.render('siting_time.html', form=form, min=min, max=max)
        return json.dumps(data)

    @expose('/save/', methods=('GET', 'POST'))
    def save_data(self):
        if request.method == 'POST':
            # try:
            request_data = json.loads(request.form.keys()[0])
            get_param = SitingTimeForm(request.args)
            try:
                teacher = current_user.id if current_user.has_role('Teacher') else SubjTable.objects(subject=request_data['subject'], group=request_data['group']).first().id
            except:
                teacher = None

            if not request_data.get('id'):
                siting = Siting(
                    subject=request_data['subject'],
                    tables=request_data['siting'],
                    classroom=request_data['classroom'],
                    group=request_data['group'],
                    teacher=teacher,
                    start_time=request_data['start_time'],
                    end_time=request_data['end_time'],
                    semester=request_data['semester'],
                )
            else:
                siting = Siting.objects.get(id=request_data.get('id'))
            siting.tables = request_data['siting']
            students = User.objects(group=siting.group)
            present = students.filter(
                id__in=[siting.tables[i] for i in siting.tables if siting.tables[i]]
            )
            not_present = students.filter(id__nin=present.distinct('id'))
            siting.students_present = present
            siting.students_not_present = not_present
            siting.students_not_present_count = len(not_present)
            siting.students_present_count = len(present)
            siting.date = get_param.date.data
            siting.pair_count = get_param.pair_count.data
            siting.time = get_param.time.data
            siting.save()
            # except:
            #     return 'error'
        return 'ok'

    def get_query(self):
        query = super(SitingView, self).get_query()
        if current_user.has_role('Teacher'):
            return query.filter(teacher=current_user.id)
        return query



class SubjectView(TranslateModelView):
    column_filters = (FilterLike(Subject.name, u'Название'),)
    list_template = 'admin/model/list_subjects.html'

    form_args = dict(
        name=dict(label=u"Название")
    )

    column_labels = dict(
        name=u"Название"
    )

    def __init__(self):
        super(SubjectView, self).__init__(model=Subject, name=u'Дисциплины', category=None, endpoint=None,
                                          url=None, static_folder=None, menu_class_name=None,
                                          menu_icon_type=None, menu_icon_value=None)

    def is_accessible(self):
        return current_user.has_role('Admin')


class GroupView(TranslateModelView):
    column_filters = (FilterLike(Group.name, u'Название'),)
    list_template = 'admin/model/list_groups.html'

    form_args = dict(
        name=dict(label=u"Название"),
        year=dict(label=u"Год поступления")
    )

    column_labels = dict(
        name=u"Название",
        year=u"Год поступления"
    )

    def __init__(self):
        super(GroupView, self).__init__(model=Group, name=u'Группы', category=None, endpoint=None,
                                        url=None, static_folder=None, menu_class_name=None,
                                        menu_icon_type=None, menu_icon_value=None)

    def is_accessible(self):
        return current_user.has_role('Admin')

    def create_form(self, obj=None):
        form_class = self.get_form()
        setattr(form_class, 'student_list', TextAreaField(u"Студенты", [validators.optional()]))
        return form_class(get_form_data(), obj=obj)

    def after_model_change(self, form, model, is_created):
        if not is_created:
            return
        for student_line in form.student_list.data.split('\n'):
            fields = re.split('\s+', student_line.strip())
            if len(fields) < 5:
                continue
            try:
                student = User(
                    last_name=fields[1],
                    first_name=fields[2],
                    second_name=fields[3],
                    login=fields[4],
                    password=uuid.uuid4().hex[:6],
                    roles=[roles.filter(name=StudentView.role)[0]],
                    group=model,
                )
                student.save()
            except Exception, e:
                pass

    @expose('/data/<group_id>', methods=('GET',))
    def get_data(self, group_id=None):
        group = Group.objects.get(id=group_id)
        group_data = dict(
            id=unicode(group.id),
            name=unicode(group.name),
            year=int(group.year),
            students=list()
        )
        students = User.objects(group=group)
        for student in students:
            student_data = {
                'id': unicode(student.id),
                'last_name': unicode(student.last_name),
                'first_name': unicode(student.first_name),
                'second_name': unicode(student.second_name),
                'photo': None,
                'login': unicode(student.login),
                'active': int(student.active),
                'group': unicode(group.id)
            }
            if student.photo.grid_id:
                student_data['photo'] = self.get_url('.api_file_view', **make_thumb_args(student.photo))
            group_data['students'].append(student_data)
        return json.dumps(group_data)


class SubjTableView(TranslateModelView):
    column_filters = (MyFilterLike(SubjTable.teacher, u'Преподаватель', model=User),
                      MyFilterLike(SubjTable.group, u'Группа', model=Group),
                      MyFilterLike(SubjTable.subject, u'Предмет', model=Subject),)
    form_overrides = dict(teacher=TeacherSelectField)

    list_template = 'admin/model/list_subjtable.html'

    form_args = dict(
        teacher=dict(label=u"Преподаватель"),
        group=dict(label=u"Группа"),
        subject=dict(label=u"Предмет"),
        semester=dict(label=u"Семестр"),
        attendance_rate=dict(label=u"Балл за посещения")
    )

    column_labels = dict(
        teacher=u"Преподаватель",
        group=u"Группа",
        subject=u"Предмет",
        semester=u"Семестр",
        attendance_rate=u"Балл за посещения"
    )

    def __init__(self):
        super(SubjTableView, self).__init__(model=SubjTable, name=u'Семестровая нагрузка', category=None, endpoint=None,
                                            url=None, static_folder=None, menu_class_name=None,
                                            menu_icon_type=None, menu_icon_value=None)

    def is_accessible(self):
        return current_user.has_role('Admin')

    @expose('/data', methods=['GET'])
    def get_data(self):
        if request.args.get('what') == 'group_semesters':
            group_id = request.args.get('group_id')

            query = {'group': group_id}
            if current_user.has_role('Teacher'):
                query['teacher'] = current_user
            semesters = SubjTable.objects(**query).distinct('semester')

            return json.dumps(map(
                lambda s: {
                    'id': unicode(s.id),
                    'title': unicode(s)
                },
                semesters
            ))

        if request.args.get('what') == 'group_semester_subjects':
            group_id = request.args.get('group_id')
            semester_id = request.args.get('semester_id')

            query = {'group': group_id, 'semester': semester_id}
            if current_user.has_role('Teacher'):
                query['teacher'] = current_user
            subjects = SubjTable.objects(**query).distinct('subject')
            return json.dumps(map(
                lambda s: {
                    'id': unicode(s.id),
                    'title': s.name
                },
                subjects
            ))


class StudentView(TranslateModelView):
    column_filters = (NameFilter(User, u'ФИО'),)

    form_args = dict(
        last_name=dict(label=u"Фамилия"),
        first_name=dict(label=u"Имя"),
        second_name=dict(label=u"Отчество"),
        photo=dict(label=u"Фото"),
        login=dict(label=u"Логин"),
        password=dict(label=u"Пароль"),
        roles=dict(label=u"Роль"),
        active=dict(label=u"Активный"),
        group=dict(label=u"Группа")
    )

    column_labels = dict(
        last_name=u"Фамилия",
        first_name=u"Имя",
        second_name=u"Отчество",
        photo=u"Фото",
        group=u'Группа'
    )
    form_excluded_columns = ('roles',)
    model = User
    name = u'Студенты'
    endpoint = 'students'
    role = 'Student'
    column_list = ['last_name', 'first_name', 'second_name', 'group', 'photo']
    list_template = 'admin/model/list_student.html'

    def __init__(self):
        super(StudentView, self).__init__(
            model=self.model, name=self.name, category=u'Учетные записи', endpoint=self.endpoint, url=None, static_folder=None,
            menu_class_name=None, menu_icon_type=None, menu_icon_value=None
        )

    def get_query(self):
        role = roles.filter(name=self.role)[0].id
        return super(StudentView, self).get_query().filter(roles=role)

    def is_accessible(self):
        return current_user.has_role('Admin')

    def on_model_change(self, form, model, is_created):
        model.roles = [roles.filter(name=self.role)[0]]


class TeacherView(StudentView):

    form_excluded_columns = ('group', 'roles')

    column_list = ['last_name', 'first_name', 'second_name', 'photo']
    list_template = 'admin/model/list_teachers.html'

    name = u'Преподаватели'
    endpoint = 'teachers'
    role = 'Teacher'


class AdminView(StudentView):

    form_excluded_columns = ('group', 'roles')

    column_list = ['last_name', 'first_name', 'second_name', 'photo']
    list_template = 'admin/model/list_admin.html'

    name = u'Администраторы'
    endpoint = 'admins'
    role = 'Admin'

    def is_accessible(self):
        return current_user.has_role('Admin')


class IndexView(AdminIndexView):
    def __init__(self):
        super(IndexView, self).__init__(name=u'Главная', category=None, endpoint=None, url=None,
                                        template='admin/index.html', menu_class_name=None, menu_icon_type=None,
                                        menu_icon_value=None)

    @login_required
    @expose(methods=['GET', 'POST'])
    def index(self):
        from mongoengine import DoesNotExist
        data = {}
        file_name = None
        form = ReportForm()
        initial_data = None
        if current_user.has_role('Teacher') or current_user.has_role('Admin'):
            if current_user.has_role('Teacher'):
                tables = Siting.objects.filter(teacher=current_user.id)
            else:
                tables = Siting.objects()
            form.group.choices = [(j.id, j.name) for j in Group.objects(id__in=[i.id for i in tables.distinct('group')])]
            form.semester.choices = [
                (j.id, '%s %d' % (u'Осень' if j.semester == 1 else u'Весна', j.start_time.year))
                for j in Semester.objects(id__in=[i.id for i in tables.distinct('semester')])
            ]
            form.subject.choices = [(j.id, j.name) for j in Subject.objects(id__in=[i.id for i in tables.distinct('subject')])]
            if request.method == 'POST':
                form2 = ReportForm(request.form)
                initial_data = request.form
                tables = tables.filter(group=form2.group.data, subject=form2.subject.data)
                try:
                    attendance_rate = SubjTable.objects.get(
                        group=form2.group.data,
                        semester=form2.semester.data,
                        subject=form2.subject.data
                    ).attendance_rate
                except DoesNotExist:
                    attendance_rate = 20
                for row in tables:
                    for stud in row.students_present:
                        if not stud in data:
                            data[stud] = dict(
                                present=0,
                                not_present=0
                            )
                        if stud.photo.grid_id:
                            data[stud]['photo'] = self.get_url('siting.api_file_view', **make_thumb_args(stud.photo))
                        data[stud]['name'] = '%s %s' % (stud.last_name, stud.first_name)
                        data[stud]['present'] += 1
                        data[stud]['rate'] = int(round(data[stud]['present'] * attendance_rate / float(data[stud]['present'] + data[stud]['not_present'])))
                    for stud in row.students_not_present:
                        if not stud in data:
                            data[stud] = dict(
                                present=0,
                                not_present=0
                            )
                        if stud.photo.grid_id:
                            data[stud]['photo'] = self.get_url('siting.api_file_view', **make_thumb_args(stud.photo))
                        data[stud]['name'] = '%s %s' % (stud.last_name, stud.first_name)
                        data[stud]['not_present'] += 1
                        data[stud]['rate'] = int(round(data[stud]['present'] * attendance_rate / float(data[stud]['present'] + data[stud]['not_present'])))

                file_name = '/static/output/%s.csv' % str(int(time())+randint(0, 9999999))
                with open(APP_ROOT+file_name, 'w') as file:
                    file.write("Студент,Присутствовал,Отсутствовал,Бал\n")
                    for i in data:
                        file.write("%s,%s,%s,%s\n" % (i, data[i]['present'], data[i]['not_present'], data[i]['rate']))

        if current_user.has_role('Student'):
            group = current_user.group
            from operator import attrgetter
            semesters = sorted(
                SubjTable.objects(group=group).distinct('semester'),
                key=attrgetter('start_time')
            )
            for semester in semesters:
                sitings = Siting.objects(group=group, semester=semester)
                semester_data = dict()
                for siting in sitings:
                    if siting.subject not in semester_data:
                        semester_data[siting.subject] = dict(
                            present=0,
                            not_present=0
                        )
                    if current_user._get_current_object() in siting.students_present:
                        semester_data[siting.subject]['present'] += 1
                    else:
                        semester_data[siting.subject]['not_present'] += 1
                    try:
                        attendance_rate = SubjTable.objects.get(
                            group=group,
                            semester=semester,
                            subject=siting.subject
                        ).attendance_rate
                    except DoesNotExist:
                        attendance_rate = 20
                    semester_data[siting.subject]['rate'] = int(round(
                        semester_data[siting.subject]['present'] * attendance_rate / float(
                            semester_data[siting.subject]['present'] + semester_data[siting.subject]['not_present'])
                    ))
                data[semester] = semester_data
            file_name = '/static/output/%s.csv' % str(int(time())+randint(0, 9999999))
            with open(APP_ROOT+file_name, 'w') as file:
                file.write("Предмет,Семестр,Присутствовал,Отсутствовал,Бал\n")
                for semester, subjects in data.iteritems():
                    for subject, values in subjects.iteritems():
                        file.write("%s,%s,%s,%s,%s\n" % (
                            subject, semester, values['present'], values['not_present'], values['rate']
                        ))
        return self.render(
            self._template,
            data=data,
            initial_data=initial_data,
            form=form,
            role=current_user.roles[0].name,
            file_name=file_name
        )


def board_position_formatter(view, context, model, name):
    if model[name] == 1:
        return u'Верх'
    else:
        return u'Низ'


class ClassroomView(TranslateModelView):
    column_list = ['name', 'horizontal_length', 'vertical_length', 'board_position']
    column_filters = (FilterLike(Classroom.name, u'Аудитория'), )
    column_formatters = dict(
        board_position=board_position_formatter,
    )
    list_template = 'admin/model/list_classroom.html'

    form_args = dict(
        name=dict(label=u"Аудитория"),
        horizontal_length=dict(label=u"Ширина"),
        vertical_length=dict(label=u"Длина"),
        board_position=dict(label=u"Положение доски")
    )

    column_labels = dict(
        name=u"Аудитория",
        horizontal_length=u"Ширина",
        vertical_length=u"Длина",
        board_position=u"Положение доски"
    )

    def __init__(self):
        super(ClassroomView, self).__init__(model=Classroom, name=u'Аудитории', category=None, endpoint=None,
                                            url=None, static_folder=None, menu_class_name=None,
                                            menu_icon_type=None, menu_icon_value=None)

    def is_accessible(self):
        return current_user.has_role('Admin')

    @expose('/new/', methods=('GET', 'POST'))
    def create_view(self):
        if request.method == 'GET':
            return_url = get_redirect_target() or self.get_url('.index_view')

            if not self.can_create:
                return redirect(return_url)

            form_class = self.get_form()
            form = form_class(get_form_data())
            return self.render(template='classroom.html', form=form, return_url=return_url)
        if request.method == 'POST':
            desk_list = request.json['desks']
            classroom = Classroom(
                name=request.json['name'],
                horizontal_length=request.json['cols'],
                vertical_length=request.json['rows'],
                board_position=request.json['board_position'],
            )
            for desk_data in desk_list:
                desk = Desk(x=desk_data['x'], y=desk_data['y'], id=ObjectId())
                classroom.desks.append(desk)
            try:
                classroom.save()
            except Exception, e:
                return json.dumps({'status': 'failed'}), 400
            else:
                return json.dumps({'status': 'success'})

    @expose('/edit/', methods=('GET', 'POST'))
    def edit_view(self):
        if request.method == 'GET':
            return_url = get_redirect_target() or self.get_url('.index_view')

            if not self.can_edit:
                return redirect(return_url)

            classroom = Classroom.objects(id=request.args.get('id')).first()
            classroom_data = dict(
                id=unicode(classroom.id),
                horizontal_length=int(classroom.horizontal_length),
                vertical_length=int(classroom.vertical_length),
                board_position=int(classroom.board_position or 0)
            )
            classroom_data['desks'] = list()
            for desk in classroom.desks:
                classroom_data['desks'].append({
                    'x': desk.x,
                    'y': desk.y,
                    'id': unicode(desk.id)
                })
            form_class = self.get_form()
            form = form_class(get_form_data(), obj=classroom)
            return self.render(
                template='classroom.html',
                form=form,
                classroom_data=classroom_data,
                return_url=return_url
            )
        if request.method == 'POST':
            desk_list = request.json['desks']
            classroom = Classroom.objects(id=request.json['classroom_id']).first()
            classroom.name = request.json['name']
            classroom.horizontal_length = int(request.json['cols'])
            classroom.vertical_length = int(request.json['rows'])
            classroom.board_position = int(request.json['board_position'])
            classroom.desks = list()
            for desk_data in desk_list:
                desk = Desk(x=desk_data['x'], y=desk_data['y'], id=ObjectId(oid=desk_data['id']))
                classroom.desks.append(desk)
            try:
                classroom.save()
            except Exception, e:
                return json.dumps({'status': 'failed'}), 400
            else:
                return json.dumps({'status': 'success'})


class SemesterSettingsView(TranslateModelView):
    def __init__(self):
        super(SemesterSettingsView, self).__init__(model=Semester, name=u'Настройки семестров', category=None,
                                                   endpoint=None, url=None, static_folder=None,
                                                   menu_class_name=None, menu_icon_type=None, menu_icon_value=None)

    form_args = dict(
        start_time=dict(label=u"Дата начала"),
        end_date=dict(label=u"Дата окончания"),
        year=dict(label=u"Год"),
        semester=dict(label=u"Семестр"),
    )

    column_labels = dict(
        semester=u"Семестр",
        start_time=u"Дата начала",
        end_date=u"Дата окончания",
    )

    form_overrides = dict(
        start_time=DateField,
        end_date=DateField,
    )

    column_formatters = dict(
        semester=lambda v, c, m, p: unicode(m),
        start_time=lambda v, c, m, p: datetime.datetime.strftime(m.start_time, '%d.%m.%Y'),
        end_date=lambda v, c, m, p: datetime.datetime.strftime(m.end_date, '%d.%m.%Y')
    )

    def is_accessible(self):
        return current_user.has_role('Admin')


