# -*- coding: utf-8 -*-
import datetime
from flask.ext.login import current_user
from flask_security.forms import NextFormMixin, verify_and_update_password, requires_confirmation
from flask.ext.wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SelectField
from models import User, Role, Group, SubjTable, Classroom, Subject, Semester
from wtforms.fields import SelectField
from wtforms.fields.html5 import IntegerField, DateField

roles = Role.objects


from wtforms_components import TimeField


class ReportForm(Form):
    group = SelectField(label=u'Группа')
    semester = SelectField(label=u'Семестр')
    subject = SelectField(label=u'Предмет')


class SitingForm(Form):
    classroom = SelectField(label=u'Аудитория')
    group = SelectField(label=u'Группа')
    # start_time = DateTimeField(label=u'Начало занятия', format='%Y-%m-%d %H:%M', widget=DateTimePickerWidget())
    # end_time = DateTimeField(label=u'Конец занятия', format='%Y-%m-%d %H:%M', widget=DateTimePickerWidget())
    subject = SelectField(label=u'Предмет')
    semester = SelectField(label=u'Семестр')
    pair_count = IntegerField(label=u'Количество пар', default=1)
    date = DateField(label=u'Дата')
    time = SelectField(label=u'Время', choices=[])


class SitingTimeForm(Form):
    pair_count = IntegerField(label=u'Количество пар', default=1)
    date = DateField(label=u'Дата')
    time = SelectField(label=u'Время', choices=[])


def siting_time_form_factory(s_id):
    form = SitingTimeForm()
    s = Semester.objects.get(id=s_id)
    form.time.choices = ['8:00', '9:30', '11:00', '12:40', '14:20', '15:50', '17:20']
    min = s.start_time.strftime('%Y-%m-%d')
    max = s.end_date.strftime('%Y-%m-%d')
    return form, min, max

def siting_form_factory(obj=None):
    form = SitingForm() if not obj else SitingForm(obj=obj)
    if not current_user.has_role('Admin'):
        table = SubjTable.objects(teacher=current_user.id).distinct('group')
        table2 = SubjTable.objects(teacher=current_user.id).distinct('subject')
        t1_id = [i.id for i in table]
        t2_id = [i.id for i in table2]
        form.group.choices = [(i.id, i.name) for i in Group.objects(id__in=t1_id)]
        form.subject.choices = [(i.id, i.name) for i in Subject.objects(id__in=t2_id)]
    else:
        form.group.choices = [(i.id, i.name) for i in Group.objects()]
        form.subject.choices = [(i.id, i.name) for i in Subject.objects()]
    form.classroom.choices = [(i.id, i.name) for i in Classroom.objects()]
    form.semester.choices = [(i.id, i) for i in Semester.objects()]
    return form



class CustomLoginForm(Form, NextFormMixin):
    login = StringField()
    password = PasswordField()
    remember = BooleanField(default=True)

    def __init__(self, *args, **kwargs):
        super(CustomLoginForm, self).__init__(*args, **kwargs)

    def validate(self):
        if not super(CustomLoginForm, self).validate():
            return False

        if self.login.data.strip() == '':
            self.login.errors.append(u'Введите логин')

        if self.password.data.strip() == '':
            self.password.errors.append(u'Введите пароль')
        try:
            self.user = User.objects.get(login=self.login.data)
        except:
            self.user = None
        if not self.user:
            self.login.errors.append(u'Пользователь с таким логином не существует')
            return False
        if not verify_and_update_password(self.password.data, self.user):
            self.password.errors.append(u'Не верный пароль')
        if requires_confirmation(self.user):
            self.login.errors.append(u'E-mail не подтвердженный')
        if not self.user.is_active():
            self.login.errors.append(u'Ваша учетная запись отключена')
        if self.errors:
            return False
        return True


class TeacherSelectField(SelectField):
    def __init__(self, label=None, validators=None, coerce=unicode, choices=None, **kwargs):
        role = roles.filter(name='Teacher').first().id
        choices = [(i.id, i) for i in User.objects(roles=role)]
        super(TeacherSelectField, self).__init__(label, validators, coerce, choices, **kwargs)

    def pre_validate(self, form):
        for v, _ in self.choices:
            if self.data == unicode(v):
                self.data = User.objects.get(id=self.data)
                break
        else:
            raise ValueError(self.gettext('Not a valid choice'))


class MyTimeField(TimeField):
    def process_formdata(self, valuelist):
        if valuelist:
            time_str = ' '.join(valuelist)
            try:
                self.data = datetime.datetime.strptime(time_str, self.format)
            except ValueError:
                self.data = None
                raise ValueError(self.gettext(self.error_msg))

