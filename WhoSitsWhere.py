# coding: utf-8

import os
from mongoengine import connect


db = connect('wsw', alias='default', host=os.environ.get('MONGOLAB_URI'))


from flask.ext.admin.menu import MenuLink
from flask.ext.security import Security, MongoEngineUserDatastore
import config
from forms import CustomLoginForm
from models import User, Role
from views import StudentView, TeacherView, AdminView, SubjectView, GroupView, SubjTableView, IndexView, ClassroomView, \
    SitingView, SemesterSettingsView
from flask import Flask, redirect
from flask.ext.admin import Admin
from flask.ext.babel import Babel


app = Flask(__name__)
babel = Babel(app)
app.config.from_object(config.DevelopmentConfig)
user_data_store = MongoEngineUserDatastore(db, User, None)
security = Security(app, user_data_store, login_form=CustomLoginForm)


admin = Admin(app, name='WhoSitsWhere', index_view=IndexView())
admin.add_link(MenuLink(u'Выход', url='/logout'))
admin.add_view(StudentView())
admin.add_view(TeacherView())
admin.add_view(AdminView())
admin.add_view(SubjectView())
admin.add_view(GroupView())
admin.add_view(SubjTableView())
admin.add_view(ClassroomView())
admin.add_view(SemesterSettingsView())
admin.add_view(SitingView())

@app.route('/')
def index():
    return redirect('/admin/')

@babel.localeselector
def get_locale():
    return 'ru'


def create_role(role):
    if not Role.objects(name=role):
        Role(name=role).save()

if __name__ == '__main__':
    for i in ['Admin', 'Student', 'Teacher']:
        create_role(i)
    role = Role.objects(name='Admin').first()
    if not User.objects(roles=role.id):
        User(login='admin', password='12345', first_name='admin',
             last_name='admin', second_name='admin', roles=[role]).save()
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 5000)))
