# coding: utf-8
from flask import request, flash
from flask.ext.admin import expose
from flask.ext.admin.form import FormOpts
from flask.ext.admin.helpers import get_redirect_target, flash_errors
from flask.ext.admin.model.helpers import get_mdict_item_or_list
from flask_admin.babel import gettext
from flask_admin.contrib.mongoengine.view import ModelView
from werkzeug.utils import redirect


class TranslateModelView(ModelView):
    @expose('/new/', methods=('GET', 'POST'))
    def create_view(self):
        """
            Create model view
        """
        return_url = get_redirect_target() or self.get_url('.index_view')

        if not self.can_create:
            return redirect(return_url)

        form = self.create_form()

        if self.validate_form(form):
            if self.create_model(form):
                if '_add_another' in request.form:
                    flash(gettext(u'Запись успешно создана.'))
                    return redirect(request.url)
                else:
                    return redirect(return_url)

        form_opts = FormOpts(widget_args=self.form_widget_args,
                             form_rules=self._form_create_rules)

        return self.render(self.create_template,
                           form=form,
                           form_opts=form_opts,
                           return_url=return_url)

    @expose('/edit/', methods=('GET', 'POST'))
    def edit_view(self):
        """
            Edit model view
        """
        return_url = get_redirect_target() or self.get_url('.index_view')

        if not self.can_edit:
            return redirect(return_url)

        id = get_mdict_item_or_list(request.args, 'id')
        if id is None:
            return redirect(return_url)

        model = self.get_one(id)

        if model is None:
            return redirect(return_url)

        form = self.edit_form(obj=model)

        if self.validate_form(form):
            if self.update_model(form, model):
                if '_continue_editing' in request.form:
                    flash(gettext(u'Запись успешно сохранена.'))
                    return redirect(request.url)
                else:
                    return redirect(return_url)

        if request.method == 'GET':
            self.on_form_prefill(form, id)

        form_opts = FormOpts(widget_args=self.form_widget_args,
                             form_rules=self._form_edit_rules)

        return self.render(self.edit_template,
                           model=model,
                           form=form,
                           form_opts=form_opts,
                           return_url=return_url)

    @expose('/delete/', methods=('POST',))
    def delete_view(self):
        """
            Delete model view. Only POST method is allowed.
        """
        return_url = get_redirect_target() or self.get_url('.index_view')

        if not self.can_delete:
            return redirect(return_url)

        form = self.delete_form()

        if self.validate_form(form):
             # id is Required()
            id = form.id.data

            model = self.get_one(id)

            if model is None:
                return redirect(return_url)

            # message is flashed from within delete_model if it fails
            if self.delete_model(model):
                flash(gettext(u'Запись успешно удалена.'))
                return redirect(return_url)
        else:
            flash_errors(form, message=u'Неудачная попытка удаления. %(error)s')

        return redirect(return_url)

    def get_empty_list_message(self):
        return gettext(u'Нет элементов в таблице.')

    def get_list(self, page, sort_column, sort_desc, search, filters,
                 execute=True):
        """
            Get list of objects from MongoEngine

            :param page:
                Page number
            :param sort_column:
                Sort column
            :param sort_desc:
                Sort descending
            :param search:
                Search criteria
            :param filters:
                List of applied filters
            :param execute:
                Run query immediately or not
        """
        query = self.get_query()

        # Filters
        if self._filters:
            for flt, flt_name, value in filters:
                if value:
                    f = self._filters[flt]
                    query = f.apply(query, f.clean(value))

        # Search
        if self._search_supported and search:
            query = self._search(query, search)

        # Get count
        count = query.count()

        # Sorting
        if sort_column:
            query = query.order_by('%s%s' % ('-' if sort_desc else '', sort_column))
        else:
            order = self._get_default_order()

            if order:
                query = query.order_by('%s%s' % ('-' if order[1] else '', order[0]))

        # Pagination
        if page is not None:
            query = query.skip(page * self.page_size)

        query = query.limit(self.page_size)

        if execute:
            query = query.all()

        return count, query