# coding: utf-8
from flask.ext.admin.contrib.mongoengine.tools import parse_like_term
from flask_admin.contrib.mongoengine.filters import FilterLike
from models import User


class MyFilterLike(FilterLike):

    def __init__(self, column, name, options=None, data_type=None, model=None):
        self.model = model
        super(MyFilterLike, self).__init__(column, name, options, data_type)

    def validate(self, value):
        return True

    def apply(self, query, value):
        term, data = parse_like_term(value)
        if self.name == u'Группа' or self.name == u'Предмет':
            f = {'name__%s' % term: data}
            flt = {'%s__in' % self.column.name: self.model.objects(**f)}
        else:
            name = data.split()
            ln = len(name)
            if ln > 3 or ln < 1:
                return query
            elif ln == 3:
                user = User.objects(last_name=name[0], first_name=name[1], second_name__startswith=name[2])
            elif ln == 2:
                user = User.objects(last_name=name[0], first_name__startswith=name[1])
            elif ln == 1:
                user = User.objects(last_name__startswith=name[0])
            flt = {'%s__in' % self.column.name: user}
        return query.filter(**flt)

class NameFilter(FilterLike):
    def apply(self, query, value):
        name = value.split()
        ln = len(name)
        if ln > 3 or ln < 1:
            return query
        elif ln == 3:
            user = User.objects(last_name=name[0], first_name=name[1], second_name__startswith=name[2])
        elif ln == 2:
            user = User.objects(last_name=name[0], first_name__startswith=name[1])
        elif ln == 1:
            user = User.objects(last_name__startswith=name[0])
        return user
