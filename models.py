# -*- coding: utf-8 -*-
from datetime import datetime

from mongoengine import Document, EmbeddedDocument, EmbeddedDocumentListField, ObjectIdField, StringField, DynamicDocument, BooleanField, ListField, ReferenceField,  IntField, ImageField, \
    DictField, DateTimeField, FloatField
from flask.ext.security import UserMixin, RoleMixin


class Siting(Document):
    students_present = ListField(ReferenceField('User'))
    students_not_present = ListField(ReferenceField('User'))
    students_present_count = IntField(min_value=0)
    students_not_present_count = IntField(min_value=0)
    subject = ReferenceField('Subject')
    group = ReferenceField('Group')
    tables = DictField()
    teacher = ReferenceField('User')
    classroom = ReferenceField('Classroom')
    start_time = DateTimeField()
    end_time = DateTimeField()
    pair_count = IntField()
    date = DateTimeField(default=datetime.now())
    time = StringField()
    semester = ReferenceField('Semester')


class Subject(Document):
    name = StringField(max_length=80, unique=True, min_length=3)

    def __unicode__(self):
        return self.name


class SubjTable(Document):
    teacher = ReferenceField('User')
    group = ReferenceField('Group')
    subject = ReferenceField('Subject')
    semester = ReferenceField('Semester')
    attendance_rate = IntField(min_value=1, default=20)


class Group(Document):
    name = StringField(max_length=80, unique=True, min_length=3)
    year = IntField(min_value=1, default=int(datetime.strftime(datetime.now(), '%Y')))

    def __unicode__(self):
        return self.name


class Role(Document, RoleMixin):
    name = StringField(max_length=80, unique=True, min_length=3)
    description = StringField(max_length=255)

    def __unicode__(self):
        return self.name


class User(DynamicDocument, UserMixin):
    login = StringField(max_length=128, unique=True, required=True)
    password = StringField(max_length=40, required=True)
    roles = ListField(ReferenceField(Role), default=[])
    active = BooleanField(default=True)
    group = ReferenceField(Group)
    first_name = StringField(max_length=128, required=True)
    last_name = StringField(max_length=128, required=True)
    second_name = StringField(max_length=128, required=True)
    photo = ImageField(thumbnail_size=(70, 70))

    def __unicode__(self):
        return u'%s %s.%s.' % (
            self.last_name, unicode(self.first_name)[0], unicode(self.second_name)[0]
        )


class Desk(EmbeddedDocument):
    id = ObjectIdField()
    x = FloatField()
    y = FloatField()


class Classroom(Document):
    name = StringField(max_length=11)
    horizontal_length = IntField(default=6)
    vertical_length = IntField(default=6)
    board_position = IntField(choices=(
        (1, u'Верх'), (2, u'Низ')
    ))
    desks = EmbeddedDocumentListField(Desk)

    def __unicode__(self):
        return self.name


def years():
    current_year = datetime.now().year
    i = current_year - 5
    while i <= current_year:
        yield (i, i)
        i += 1


class Semester(Document):
    # year = IntField(choices=years())
    semester = IntField(choices=(
        (1, u'Осень'), (2, u'Весна')
    ), required=True)
    start_time = DateTimeField(required=True)
    end_date = DateTimeField(required=True)

    def __unicode__(self):
        s = u'Осень ' if self.semester == 1 else u'Весна '
        return s + unicode(self.start_time.year)
