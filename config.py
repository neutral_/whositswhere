# -*- coding: utf-8 -*-
import os


class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'fc7253fa85e5a4faa285b0bc41c2bbdbd18c293f'

    SECURITY_REGISTERABLE = True
    SECURITY_RECOVERABLE = False
    SECURITY_POST_LOGIN_VIEW = '/'
    SECURITY_POST_LOGOUT_VIEW = '/login'
    SECURITY_SEND_REGISTER_EMAIL = False

    UPLOAD_FOLDER = '/tmp/'  # in ~/

    SECURITY_TRACKABLE = False
    BABEL_DEFAULT_LOCALE = 'ru'
    # LANGUAGES = {
    #     'ru': 'ru_RU',
    # }


class DevelopmentConfig(Config):
    DEBUG = True
    DEBUG_TB_PANELS = (
        'flask.ext.debugtoolbar.panels.versions.VersionDebugPanel',
        'flask.ext.debugtoolbar.panels.timer.TimerDebugPanel',
        'flask.ext.debugtoolbar.panels.headers.HeaderDebugPanel',
        'flask.ext.debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
        'flask.ext.debugtoolbar.panels.template.TemplateDebugPanel',
        'flask.ext.debugtoolbar.panels.logger.LoggingPanel',
        'flask.ext.mongoengine.panels.MongoDebugPanel'
    )
    DEBUG_TB_INTERCEPT_REDIRECTS = False

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

